#include "BSNode.h"
#include <iostream>

//C'Tor
BSNode::BSNode(std::string data)
{
	_data = data;
	_left = nullptr;
	_right = nullptr;
	_count = 1;
}


//Copy C'Tor
BSNode::BSNode(const BSNode& other)
{
	if (other.isLeaf())
	{
		_data = other.getData();
		_count = other.getCount();
		_left = nullptr;
		_right = nullptr;
		_count = other.getCount();
	}
	else if (other.getRight() != nullptr && other.getLeft() == nullptr)// Has Right branch
	{
		_data = other.getData();
		_count = other.getCount();
		_right = new BSNode(*other.getRight());
		_left = nullptr;
	}
	else if (other.getLeft() != nullptr && other.getRight() == nullptr)// Has right branch
	{
		_data = other.getData();
		_count = other.getCount();
		_left = new BSNode(*other.getLeft());
		_right = nullptr;
	}
	else// Has 2 branches
	{
		_data = other.getData();
		_count = other.getCount();
		_right = new BSNode(*other.getRight());
		_left = new BSNode(*other.getLeft());
	}
}

//D'Tor
BSNode::~BSNode()
{
	if (this != nullptr)
	{
		delete (_right);
		delete (_left);
	}
}


/*
This function inserts a new leaf to the tree according to the BST
In: new value
*/
void BSNode::insert(std::string value)
{
	if (value.compare(this->getData()) > 0)//Add to right branch if bigger
	{
		if (_right != nullptr)//If already has branch
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
		}
	}
	else if(value.compare(this->getData()) < 0)//Add to left branch if smaller
	{
		if (_left != nullptr)//If already has branch
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode(value);
		}
	}
	else//Equal
	{
		_count++;
	}
}

/*
This function CHANGES an existing tree so it will be identical to another tree
*/
BSNode& BSNode::operator=(const BSNode& other)
{
	if (_right == nullptr)
	{
		delete _right;
	}
	if (_left == nullptr)
	{
		delete _left;
	}

	if (other.isLeaf())
	{
		_data = other.getData();
		_count = other.getCount();
		_left = nullptr;
		_right = nullptr;
		_count = other.getCount();
	}
	else if (other.getRight() != nullptr && other.getLeft() == nullptr)// Has Right branch
	{
		
		_data = other.getData();
		_count = other.getCount();
		_right = new BSNode(*other.getRight());
		_left = nullptr;
	}
	else if (other.getLeft() != nullptr && other.getRight() == nullptr)// Has right branch
	{
		_data = other.getData();
		_count = other.getCount();
		_left = new BSNode(*other.getLeft());
		_right = nullptr;
	}
	else// Has 2 branches
	{
		_data = other.getData();
		_count = other.getCount();
		_right = new BSNode(*other.getRight());
		_left = new BSNode(*other.getLeft());
	}
	return* this;
}



/*
This function Checks if the node is a leaf
Out: True if leaf, false otherwise
*/
bool BSNode::isLeaf() const
{
	return (_right == nullptr && _left == nullptr);
}



/*The next 3 functions are standart Getters
In: None
Out: the requested member
*/
std::string BSNode::getData() const
{
	return _data;
}
BSNode* BSNode::getLeft() const 
{
	return _left;
}
BSNode* BSNode::getRight() const
{
	return _right;
}
int BSNode::getCount() const
{
	return _count;
}



/*
This function searches for a value in the binary tree
In: val we want to search
Out: True if found, false otherwise
*/
bool BSNode::search(std::string val) const
{

	if (val.compare(this->_data) == 0)//If Equal return true
	{
		return true;
	}
	else if (val.compare(this->_data) > 0)//If bigger send to right branch
	{
		if (_right != nullptr)//If branch exists
		{
			return _right->search(val);
		}
		else
		{
			return false;
		}
	}
	else//If smaller send to left branch
	{
		if (_left != nullptr)//If branch exists
		{
			return _left->search(val);
		}
		else
		{
			return false;
		}
	}
}



/*
This function Checks the length of a given tree
In: tree root
Out: Height of the tree
*/
int BSNode::getHeight() const
{
	int rightHeight = 0, leftHeight = 0;
	//get the Height of each of the beanches
	if (_left != nullptr)
	{
		leftHeight = _left->getHeight();;
	}
	if (_right != nullptr)
	{
		rightHeight = _right->getHeight();
	}
	  
	if (this->isLeaf())//If this node is the leaf
	{
		return 1;
	}
	//Return the taller branch
	else if (rightHeight > leftHeight)
	{
		return rightHeight + 1;
	}
	else if (rightHeight < leftHeight)
	{
		return leftHeight + 1;
	}
	else//Return one of them because they are equal
	{
		return leftHeight;//Can also be rightHeight
	}
}



/*
This function return the depth of the this compare to the parameter
In: Root that will be Checked from
Out: diffrence between this and root
*/
int BSNode::getDepth(const BSNode& root) const
{
	int temp = 0;
	if (this->getData().compare(root.getData()) == 0)//If not current one
	{
		return 1;
	}
	else if (this->getData().compare(root.getData()) > 0)//Go to right branch
	{
		if (root.getRight() != nullptr)
		{
			temp = (*this).getDepth(*root.getRight());
			if (temp > 0)//If currect branch
			{
				return temp + 1;
			}
			return 0;
		}
		else
		{
			return 0;
		}
		
	}
	else//Go to right branch
	{
		if (root.getLeft() != nullptr)
		{
			temp = (*this).getDepth(*root.getLeft());
			if (temp > 0)//If currect branch
			{
				return temp + 1;
			}
			return 0;
		}
		else
		{
			return 0;
		}
	}

}



/*
This function prints the tree by inorder
*/
void BSNode::printNodes() const
{
	if (this == nullptr)
	{
		
	}
	else
	{
		/* first recur on left branch */
		this->getLeft()->printNodes();

		/* then print the data of node */
		std::cout << this->_data << '(' << this->getCount() << ')' << std::endl;

		/* now recur on right branch */
		this->getRight()->printNodes();

	}
}


