#include "SimpleClass.h"

//C'Tors
SimpleClass::SimpleClass(int value)
{
	_value = value;
}
SimpleClass::SimpleClass()
{
	_value = 0;
}

//Getter
int SimpleClass::getValue() const
{
	return _value;
}

//Override bigger smaller operator for class
bool SimpleClass::operator>(const SimpleClass& other) const
{
	if (this->_value > other.getValue())
	{
		return true;
	}
	else
	{
		return false;
	}
}



std::ostream& operator<<(std::ostream& os, const SimpleClass &toPrint)
{
	os << toPrint.getValue();
	return os;
}