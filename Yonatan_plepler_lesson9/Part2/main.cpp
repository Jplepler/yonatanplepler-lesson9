
#include <iostream>
#include "SimpleClass.h"
#include "Templates.h"


int main() {

	///////////////////////////////////MY CODE//////////////////////////
	SimpleClass s1(2);
	SimpleClass s2(5);
	SimpleClass s3(1);
	SimpleClass s4(10);
	SimpleClass s5(7);
	//check compare
	
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<SimpleClass>(s2, s1) << std::endl;
	std::cout << compare<SimpleClass>(s1, s2) << std::endl;
	std::cout << compare<SimpleClass>(s1, s1) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;
	const int arr_size = 5;
	SimpleClass SimpleClassArr[arr_size] = { s1, s2, s3, s4, s5 };
	bubbleSort<SimpleClass>(SimpleClassArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << SimpleClassArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<SimpleClass>(SimpleClassArr, arr_size);
	std::cout << std::endl;









//////////////////////Given Code//////////////////////////////
//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}