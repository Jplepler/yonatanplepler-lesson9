#pragma once

template <typename T>
int compare(T x, T y)
{
	return (x > y ? -1 : (y > x ? 1 : 0));
}


/*
This function uses bubble algorithm to sort the array
In: array and its size
*/
template <typename T>
void bubbleSort(T arr[], int n)
{
	int i, j;
	T temp;
	for (i = 0; i < n; i++)//Repeat n times
	{
		for (j = i + 1; j < n; j++)//Each time go from starting index and bubble the biggest value to the end
		{
			if (arr[i] > arr[j])
			{
				//Swap
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
}


/*
This function print all value in an array
In: array and its size
*/
template <typename T>
void printArray(T arr[], int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}