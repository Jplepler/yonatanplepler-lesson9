#pragma once
#include <iostream>
class SimpleClass
{
public:
	SimpleClass(int value);
	SimpleClass();
	int getValue() const;
	bool operator>(const SimpleClass& other) const;
	friend std::ostream& operator<<(std::ostream& os, const SimpleClass& toPrint);

private:
	int _value;
};