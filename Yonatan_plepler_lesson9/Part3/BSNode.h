#pragma once
#include <iostream>
#include <string>
template <typename T>
class BSNode
{
public:
	//C'Tor
	BSNode(T data)
	{
		_data = data;
		_left = nullptr;
		_right = nullptr;
		_count = 1;
	}
	//Copy C'Tor
	BSNode(const BSNode& other)
	{
		if (other.isLeaf())
		{
			_data = other.getData();
			_count = other.getCount();
			_left = nullptr;
			_right = nullptr;
			_count = other.getCount();
		}
		else if (other.getRight() != nullptr && other.getLeft() == nullptr)// Has Right branch
		{
			_data = other.getData();
			_count = other.getCount();
			_right = new BSNode(*other.getRight());
			_left = nullptr;
		}
		else if (other.getLeft() != nullptr && other.getRight() == nullptr)// Has right branch
		{
			_data = other.getData();
			_count = other.getCount();
			_left = new BSNode(*other.getLeft());
			_right = nullptr;
		}
		else// Has 2 branches
		{
			_data = other.getData();
			_count = other.getCount();
			_right = new BSNode(*other.getRight());
			_left = new BSNode(*other.getLeft());
		}
	} 
	//D'Tor
	~BSNode()
	{

		if (isLeaf())
		{
			//Pointers are already nullptr
		}
		else if (getRight() != nullptr)// Has Right branch
		{
			if (getRight()->isLeaf())//If right is leaf
			{
				delete getRight();
			}
			else
			{
				getRight()->~BSNode();
				delete getRight();

			}
			_right = nullptr;
		}
		else if (getLeft() != nullptr)// Has right branch
		{
			if (getLeft()->isLeaf())//If right is leaf
			{
				delete getLeft();
			}
			else
			{
				getLeft()->~BSNode();
				delete getLeft();
			}
			_left = nullptr;
		}
		else// Has 2 branches
		{
			if (getRight()->isLeaf())//If right is leaf
			{
				delete getRight();
			}
			else
			{
				getRight()->~BSNode();
				delete getRight();
			}
			_right = nullptr;

			if (getLeft()->isLeaf())//If right is leaf
			{
				delete getLeft();
			}
			else
			{
				getLeft()->~BSNode();
				delete getLeft();
			}
			_left = nullptr;
		}
	}
	
	/*
	This function inserts a new leaf to the tree according to the BST
	In: new value
	*/
	void insert(T value)
	{
		if (value > this->getData())//Add to right branch if bigger
		{
			if (_right != nullptr)//If already has branch
			{
				_right->insert(value);
			}
			else
			{
				_right = new BSNode(value);
			}
		}
		else if (value < this->getData())//Add to left branch if smaller
		{
			if (_left != nullptr)//If already has branch
			{
				_left->insert(value);
			}
			else
			{
				_left = new BSNode(value);
			}
		}
		else//Equal
		{
			_count++;
		}
	}
	/*
	This function CHANGES an existing tree so it will be identical to another tree
	*/
	BSNode& operator=(const BSNode& other)
	{
		if (_right == nullptr)
		{
			delete _right;
		}
		if (_left == nullptr)
		{
			delete _left;
		}

		if (other.isLeaf())
		{
			_data = other.getData();
			_count = other.getCount();
			_left = nullptr;
			_right = nullptr;
			_count = other.getCount();
		}
		else if (other.getRight() != nullptr)// Has Right branch
		{

			_data = other.getData();
			_count = other.getCount();
			_right = new BSNode(*other.getRight());
			_left = nullptr;
		}
		else if (other.getLeft() != nullptr)// Has right branch
		{
			_data = other.getData();
			_count = other.getCount();
			_left = new BSNode(*other.getLeft());
			_right = nullptr;
		}
		else// Has 2 branches
		{
			_data = other.getData();
			_count = other.getCount();
			_right = new BSNode(*other.getRight());
			_left = new BSNode(*other.getLeft());
		}
		return*this;
	}

	/*
	This function Checks if the node is a leaf
	Out: True if leaf, false otherwise
	*/
	bool isLeaf() const
	{
		return (_right == nullptr && _left == nullptr);
	}
	/*The next 3 functions are standart Getters
	In: None
	Out: the requested member
	*/
	T getData() const
	{
		return _data;
	}
	BSNode* getLeft() const
	{
		return _left;
	}
	BSNode* getRight() const
	{
		return _right;
	}
	int getCount() const
	{
		return _count;
	}

	/*
	This function searches for a value in the binary tree
	In: val we want to search
	Out: True if found, false otherwise
	*/
	bool search(T val) const
	{

		if (val.compare(this->_data) == 0)//If Equal return true
		{
			return true;
		}
		else if (val.compare(this->_data) > 0)//If bigger send to right branch
		{
			if (_right != nullptr)//If branch exists
			{
				return _right->search(val);
			}
			else
			{
				return false;
			}
		}
		else//If smaller send to left branch
		{
			if (_left != nullptr)//If branch exists
			{
				return _left->search(val);
			}
			else
			{
				return false;
			}
		}
	}
	/*
	This function Checks the length of a given tree
	In: tree root
	Out: Height of the tree
	*/
	int getHeight() const
	{
		int rightHeight = 0, leftHeight = 0;
		//get the Height of each of the beanches
		if (_left != nullptr)
		{
			leftHeight = _left->getHeight();;
		}
		if (_right != nullptr)
		{
			rightHeight = _right->getHeight();
		}

		if (this->isLeaf())//If this node is the leaf
		{
			return 1;
		}
		//Return the taller branch
		else if (rightHeight > leftHeight)
		{
			return rightHeight + 1;
		}
		else if (rightHeight < leftHeight)
		{
			return leftHeight + 1;
		}
		else//Return one of them because they are equal
		{
			return leftHeight;//Can also be rightHeight
		}
	}
	/*
	This function return the depth of the this compare to the parameter
	In: Root that will be Checked from
	Out: diffrence between this and root
	*/
	int getDepth(const BSNode& root) const
	{
		int temp = 0;
		if (this->getData().compare(root.getData()) == 0)//If not current one
		{
			return 1;
		}
		else if (this->getData().compare(root.getData()) > 0)//Go to right branch
		{
			if (root.getRight() != nullptr)
			{
				temp = (*this).getDepth(*root.getRight());
				if (temp > 0)//If currect branch
				{
					return temp + 1;
				}
				return 0;
			}
			else
			{
				return 0;
			}

		}
		else//Go to right branch
		{
			if (root.getLeft() != nullptr)
			{
				temp = (*this).getDepth(*root.getLeft());
				if (temp > 0)//If currect branch
				{
					return temp + 1;
				}
				return 0;
			}
			else
			{
				return 0;
			}
		}

	}
	/*
	This function prints the tree by inorder
	*/
	void printNodes() const
	{
		if (this == NULL)
		{

		}
		else
		{
			/* first recur on left branch */
			this->getLeft()->printNodes();

			/* then print the data of node */
			std::cout << this->_data << '(' << this->getCount() << ')' << std::endl;

			/* now recur on right branch */
			this->getRight()->printNodes();

		}
	}

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count;
};