#include "BSNode.h"
#define ARR_SIZE 15
using namespace std;
int main()
{
	int i = 0;
	int arr1[ARR_SIZE] = {10, 100, 1, 0 , 8, 78, 88, 54, 34, 19, 78, 11, 64, 77, 39};
	float arr2[ARR_SIZE] = { 1.5, 1.1, 2.7, 4.4, 3.9, 6.9, 4.20, 0.123, 12.34, 6.66, 0.25, 0.125, 0.001, 9.99, 12.0 };
	

	//Print Both arrays
	cout << "The Two arrays: " << endl;
	for (i = 0; i < ARR_SIZE; i++)
	{
		cout << arr1[i] << " ";
	}
	cout << endl;
	for (i = 0; i < ARR_SIZE; i++)
	{
		cout << arr2[i] << " ";
	}

	//Insert both arrays into BSTs
	BSNode<int> root1(arr1[0]);
	BSNode<float> root2(arr2[0]);
	for (i = 1; i < ARR_SIZE; i++)
	{
		root1.insert(arr1[i]);
	}
	for (i = 1; i < ARR_SIZE; i++)
	{
		root2.insert(arr2[i]);
	}


	//Print BSTs
	cout << endl;
	cout << endl;
	root1.printNodes();
	cout << endl;
	cout << endl;
	root2.printNodes();
	return 0;
}